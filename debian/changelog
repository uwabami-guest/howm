howm (1.5.3-1) unstable; urgency=medium

  * New upstream version 1.5.3
  * Refresh patches

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 06 Feb 2025 09:47:35 +0900

howm (1.5.2-1) unstable; urgency=medium

  * New upstream version 1.5.2
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Update lintian-overrides: National encoding issue
  * Refresh patches

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 10 Jan 2025 19:59:15 +0900

howm (1.5.1-3) unstable; urgency=medium

  * Rebuild against newer dh-elpa (Closes: #1077108)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 28 Jul 2024 15:24:57 +0900

howm (1.5.1-2) unstable; urgency=medium

  * Fix elisp installation(Closes: #1067875)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 27 Mar 2024 21:01:31 +0900

howm (1.5.1-1) unstable; urgency=medium

  * Bump Standards-Version to 4.6.2 (no changes needed)
  * Update d/watch, use github as upstream
  * Refresh patches
  * New upstream version 1.5.1

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 27 Mar 2024 19:11:09 +0900

howm (1.4.8-1) unstable; urgency=medium

  * New upstream version 1.4.8 (Closes: #1016609)
  * Refresh patches
  * Add lintian-overrrides: suppress national-encoding issues
  * Bump debhelper compatibility level to 13
  * d/copyright: update year
  * Update d/rules, override dh_autoreconf
  * d/control: provides elpa-howm (Closes: #1016608)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 04 Aug 2022 16:09:57 +0900

howm (1.4.7-1) unstable; urgency=medium

  [ KURASHIKI Satoru ]
  * New upstream version 1.4.7
  * d/control: Bump Standard Version 4.5.1
  * Use secure URI in Homepage field.

  [ Youhei SASAKI ]
  * Bump debhelper compatibility level to 12

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 08 Jan 2021 00:29:24 +0900

howm (1.4.6-2) unstable; urgency=medium

  * d:control: Update Vcs-* field

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 19 Apr 2020 13:05:26 +0900

howm (1.4.6-1) unstable; urgency=medium

  * New upstream version 1.4.6
  * Refresh patches

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 23 Jan 2020 14:25:15 +0900

howm (1.4.5-1) unstable; urgency=medium

  * New upstream version 1.4.5
  * d/elpa: Add howm-{menu,lang}-fr.el
  * Update/Refresh patches
  * d/control: Bump Standard Version 4.4.1
  * Fix debian/NEWS syntax
  * d/control:
    - use debhelper-compat (=12)
    - add Rules-Requires-Root: no
  * Add d/salsa-ci.yml

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 22 Dec 2019 16:08:29 +0900

howm (1.4.4-3) unstable; urgency=medium

  * d/rules: Drop unnecessary dh option
  * d/control: Fix Vcs-Browser field
  * d/control: Bump Standard-Version 4.2.1
  * d/{control,compat}: Bump compat 11

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 11 Sep 2018 03:54:02 +0900

howm (1.4.4-2) unstable; urgency=medium

  * Update Vcs-* field

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 29 Jan 2018 02:00:06 +0900

howm (1.4.4-1) unstable; urgency=medium

  * Move to unstable
  * Bump Standard Version: 4.1.1
  * Bump compat: 10
  * Use dh-elpa
  * Update Vcs field
  * Bump uscan version
  * Remove autotools output files
  * Drop conflicts: emacsen-common
  * Fix up old build scripts
  * Refresh patches
  * Drop Debian's startup file

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 18 Nov 2017 17:51:21 +0900

howm (1.4.4-1~exp1) experimental; urgency=medium

  * New upstream version 1.4.4
  * Refresh patches
  * Update debian/changelog
  * Fix typo in README.Debian
  * Bump Standard version: 3.9.8

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 07 Jan 2017 18:14:30 +0900

howm (1.4.3-1) unstable; urgency=medium

  * Merge upstream branch
  * Imported Upstream version 1.4.3
  * Refresh patches
  * Drop dh-autoreconf
  * Bump Compat: 9
  * Bump Standard version: 3.9.6
  * Change upstream URL
  * Update copyright format
  * Update Vcs-Browser: use cgit instead of gitweb
  * Add patch: 0004-Fix-privacy-breach-logo.patch
  * Refresh patches
  * Drop emacs <=23 support

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 08 Jan 2016 21:07:50 +0900

howm (1.4.2-2) unstable; urgency=medium

  * Handling new emacsen-common
    - Install a compat file with emacsen-compat                               |
    - Add conflicts: emacsen-common (<< 2.0.0)                                |
    - Add postinst/prerm

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 09 Mar 2014 15:58:58 +0900

howm (1.4.2-1) unstable; urgency=medium

  * Imported Upstream version 1.4.2
  * Use canonical Vcs-* field
  * Bump Standard Version: 3.9.5
  * Update Build-Depends, Depends
  * Remove unneeded dh-autoreconf
  * Update emacsen scripts
  * Update patches:
    + Fix auto-mode-alist regexp (Closes: #712837)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 23 Feb 2014 16:42:33 +0900

howm (1.4.1-2) unstable; urgency=low

  * Bump Standard Version: 3.9.4
  * Drop obsolete DM-Upload-Allowed
  * Use dh_autoreconf
    + Add debian/patches/0002-Prepare-html-link-for-doc-base.patch
      in order to generate/update html documants
  * Add doc-base support

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 08 May 2013 19:03:40 +0900

howm (1.4.1-1) experimental; urgency=low

  * Imported Upstream version 1.4.1
  * Refresh patch

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 13 Jan 2013 04:24:24 +0900

howm (1.4.0rel-1) unstable; urgency=low

  * Import new upstream: 1.4.0
  * Fix wrong version numbering and avoid epoch (Closes: #664858)
  * Update debian/watch

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 18 Aug 2012 09:51:41 +0900

howm (1.4.0rc2-2) unstable; urgency=low

  * autoload howm if only howm package is installed

 -- Kiwamu Okabe <kiwamu@debian.or.jp>  Tue, 29 May 2012 22:59:10 +0900

howm (1.4.0rc2-1) unstable; urgency=low

  * New Upstream version: 1.4.0rc2
  * Bump standard  version: 3.9.3
    - Drop quilt from Build-Depends
  * Update debian/watch: follow upstream uri change
  * Change patch handling:
    - echo "unapply-patches" > debian/soruce/local-options
    - Update patch: 0001-Update-test-case-for-Debian-Package.patch
  * Remove autoload, global-set-key in /etc/emacs/site-start.d/50howm.el
    - The "global-set-key" is overmuch setting. remote it (Closes: #664801)
    - Need bacword-compatiblity flag before "autoload".

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 21 Mar 2012 13:52:32 +0900

howm (1.3.9.2rc1-1) unstable; urgency=low

  * New Upstream version: 1.3.9.2rc1
    - fix old-style backqoutes

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 22 Jan 2011 04:08:01 +0900

howm (1.3.9.1-1) unstable; urgency=low

  * New Upstream release: 1.3.9.1
    - This release fixed bug for emacs24
  * Exclude howm-vars.el from byte compile list
    - Because howm-vars.elc use old-style backquotes

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 12 Jan 2011 14:10:18 +0900

howm (1.3.9-1) unstable; urgency=low

  * New upstream release: 1.3.9
  * Bump Standard Version: 3.9.1
  * add DM-Upload-Allowed: yes

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 02 Jan 2011 01:52:15 +0900

howm (1.3.8-1) unstable; urgency=low

  * Initial Release (Closes: #584207)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 02 Jun 2010 15:33:39 +0900
